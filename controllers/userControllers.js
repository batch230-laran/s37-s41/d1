const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName:reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
		// 10 - salt
	})

	return newUser.save().then((user,error) => {
		if(error) {
			return false;
		}

		else{
			return true
		}
	}
)
}


module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		}
		else {
			return false;
		}
	})
}



module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			// compareSync is a bcrypt function compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);  // true or false

			if(isPasswordCorrect){
				// Let's give the user a token an access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password does not match, else
				return false;
			}
		}
	})
}



// ----------------------------- Activity



module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody._id).then((result, error) => {
		if(error){
			return false;
		}
		else{
				result.password = "*********";
				return result;
			}
	});
}



// ----------------------------- s41

module.exports.getProfile = (request, response) => {

	// will contain your decoded token
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);


	return User.findById(userData.id).then(result => {
		result.password = "*****";
		respnse.send(result);
		
	})
}



