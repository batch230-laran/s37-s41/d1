const mongoose = require("mongoose");
const Course = require("../models/course.js");

// Function for adding a course
// 2. Update the "addCourse" controller methos to implement admin authentication for creating a course.
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by user that is not admin

/*
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse, error) => 
	{
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}
*/


// GET ALL COURSE
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
}



// GET ALL ACTIVE COURSES
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}


// GET SPECIFIC COURSE
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}





// ------------------------------- ACTIVITY s39


// 2. Update the "addCourse" controller methos to implement admin authentication for creating a course.
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by user that is not admin


module.exports.addCourse = (reqBody, addedCourse) => {
	if(addedCourse.isAdmin == true) {
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save().then((newCourse, error) => {
			if(error) {
				return error;
			}
			else {
				return newCourse;
			}
		})
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}




// ------------------------------- ACTIVITY s40


module.exports.archiveCourse = (courseId, archiveData) => {
	if(archiveData.isAdmin == true) {
		return Course.findByIdAndUpdate(courseId, {
			isActive: archiveData.course.isActive,
		}).then((result, error) => {
			if(error) {
				return false;
			}
			else {
				return true
			}
		})
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {
			return value});
	}
}



// ------------------------------- ENROLL FEATURE

module.exports.enroll = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	let courseName = await Course.findById(request.body.courseId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header (request header contains the user token)
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		courseID: request.body.courseID,
		courseName: courseName
	}
	console.log(newData);

	// a user document is updated if we received a "true" value 
	let isUserUpdated = await User.findById(newData.userId).then (user => {
		user.enrollments.push({
			courseId: newData.courseId,
			courseName: courseName
		});

		// Save the updated user information from the database
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isUserUpdated);


let isCourseUpdated = await Course.findById(newData.courseId).then(course => {
	course.enrollees.push({
		userId: newData.userId,
		email: newData.email
	})

	// Mini Activity - 
	// [1] Create a condition that if the slots is already zero, no deduction os lsot will happen and 
	// [2] it should have a message in the terminal that the slot is already zero
	// [3] Else if the slot is negative value, it should make the slot value to zero

	// Minus the slots available by 1
	// course.slots = course.slots - 1;

	return course.save()
	.then(result => {
		console.log(result);
		return true;
	})
	.catch(error => {
		console.log(error);
		return false;
	})
})
console.log(isCourseUpdated);

// Condition will check if the both "user" and "course" document has been updated

// TERNARY OPERATOR
(isUserUpdated == true && isCourse == true)? response.send(true) : response.send(false);

/*
if(isUserUpdated == true && is courseUpdated == true){
	response.send(true);
}
else{
	response.send(false);
}
*/

}



