/*

GitBash Dependencies:
npm init -y
npm install express
npm install mongoose
npm install cors

*/



const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");

// to create an express server/application
const app = express();


// Middlewares - allows to bridge our backend application (server) to our front end 
// to allow cross origin resources sharing
app.use(cors());


// to read json objects
app.use(express.json());


// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/user", userRoutes);
app.use("/courses", courseRoutes);

// Connect to our MongoDb database
mongoose.connect("mongodb+srv://admin:admin@batch230.gwcbkyy.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Laran-Mongo DB Atlas"));


app.listen(process.env.PORT || 4000, () => 
	{ console.log(`API is now online on port ${process.env.PORT || 4000}`)
});
