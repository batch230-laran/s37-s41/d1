const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");


// Activity
// 1. Update the "course" route to implement user authentcation for the admin when creating a course.



// ------------------------------- ACTIVITY s39



router.post("/create", auth.verify, (request, response) => {
	const addedCourse = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	courseControllers.addCourse(request.body, addedCourse).then(
		resultFromController => response.send(resultFromController))
})






/*
// CREATING A COURSE
router.post("/create", (request, response) => {
	courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))

})

*/

// GET ALL COURSE
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})



// GET ALL ACTIVE COURSES
router.get("/active", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})



// GET SPECIFIC COURSE
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
})



// PATCH
router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})



// ------------------------------- ACTIVITY s40

router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const archiveData = {
		course: request.body, 	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.archiveCourse(request.params.courseId, archiveData).then(resultFromController => {
		response.send(resultFromController)
	})
})





module.exports = router;




